import React from 'react'
import {
    TextStyle,
    TextH1Style,
    TextH2Style,
    TextH3Style,
    TextH4Style,
    TextH5Style,
    TextPStyle,
    TextSpanStyle
} from './style'

const H1Style = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextH1Style align={align} color={color} weight={weight}>
                {children}
            </TextH1Style>
        </React.Fragment>
    )
}

const H2Style = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextH2Style align={align} color={color} weight={weight}>
                {children}
            </TextH2Style>
        </React.Fragment>
    )
}

const H3Style = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextH3Style align={align} color={color} weight={weight}>
                {children}
            </TextH3Style>
        </React.Fragment>
    )
}

const H4Style = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextH4Style align={align} color={color} weight={weight}>
                {children}
            </TextH4Style>
        </React.Fragment>
    )
}

const H5Style = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextH5Style align={align} color={color} weight={weight}>
                {children}
            </TextH5Style>
        </React.Fragment>
    )
}

const PStyle = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextPStyle align={align} color={color} weight={weight}>
                {children}
            </TextPStyle>
        </React.Fragment>
    )
}

const SpanStyle = props => {
    const { align, color, weight, children } = props
    return (
        <React.Fragment>
            <TextSpanStyle align={align} color={color} weight={weight}>
                {children}
            </TextSpanStyle>
        </React.Fragment>
    )
}

export class Text extends React.Component {
    static h1 = H1Style
    static h2 = H2Style
    static h3 = H3Style
    static h4 = H4Style
    static h5 = H5Style
    static p = PStyle
    static span = SpanStyle
    render() {
        const { className, align, self, left, right, children } = this.props
        return (
            <React.Fragment>
                <TextStyle
                    className={className}
                    align={align}
                    self={self}
                    left={left}
                    right={right}
                >
                    {children}
                </TextStyle>
            </React.Fragment>
        )
    }
}

TextStyle.defaultProps = {
    align: 'center',
    self: 'none',
    left: '0',
    right: '0'
}

TextH1Style.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}

TextH2Style.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}

TextH3Style.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}

TextH4Style.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}

TextH5Style.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}

TextPStyle.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}

TextSpanStyle.defaultProps = {
    align: 'center',
    color: 'black',
    weight: 'normal'
}
