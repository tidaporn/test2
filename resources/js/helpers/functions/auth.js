import { context } from '../../context'
import { removeLocalStorage } from '../../helpers'

export const logout = async () => {
    removeLocalStorage('userAuth')
    context.setContext({
        routeHistory: ''
    })
}
