import React from 'react'
import {
    Container,
    Row,
    Card,
    Modal,
    ModalBody,
    ModalFooter,
    Col
} from 'reactstrap'
import { InputFile, Image, ButtonSquare, Flex, Text } from '../../components'
import { Common } from '../../assets'
import Form from 'react-jsonschema-form'
import { context } from '../../context'
import SignaturePad from 'react-signature-pad'
import {
    faFileExport,
    faFilePdf,
    faCheck,
    faTimes
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { jobController } from '../../api'
export class JobContainer extends React.Component {
    state = {
        coverPhoto: null,
        isPhoto: false,
        isignaturePad: false,
        signatureRef: React.createRef(),
        signatureSrc: ''
    }

    form = {
        onClickUploadPhoto: ev => {
            const { files } = ev.target
            const reader = new FileReader()
            if (files[0].type.includes('image')) {
                reader.onloadend = e => {
                    this.setState({
                        coverPhoto: reader.result,
                        isPhoto: true
                    })
                }
                reader.readAsDataURL(files[0])
            } else {
                alert('Invalid image format.')
            }
        },
        onSubmitSaveJob: ({ formData }) => {
            const { job } = context.state
            const { coverPhoto } = this.state
            const newJob = job
            newJob.photo = {
                attributes: {
                    image: coverPhoto.split(',')[1]
                }
            }
            job.propertygroup.map(items => {
                if (items.property.attributes) {
                    items.property.attributes.data =
                        formData[items.property.attributes.name]
                } else {
                    items.property.map(items => {
                        items.attributes.data = formData[items.attributes.name]
                    })
                }
                return items
            })
            console.log('job', newJob)
            context.setContext({ job: newJob })
        },
        onClickSaveJob: () => {
            const { job } = context.state
            this.updateJob(job)
        },
        onClickCancelJob: () => {
            window.confirm('Any unsaved changes will be lost.')
                ? this.props.history.push('/home')
                : null
        },
        onClickSaveSignature: () => {
            const { signatureRef } = this.state
            console.log('save sign')
            this.setState({
                signatureSrc: signatureRef.current.toDataURL()
            })
            this.toggle.onToggleSignaturePad()
        },
        onClickDeleteSignature: () => {},
        onClickClearSignature: () => {
            const { signatureRef } = this.state
            signatureRef.current.clear()
        },
        onRenderSignature: props => {
            const { signatureSrc } = this.state
            console.log('asdsadad', props)
            return (
                <React.Fragment>
                    {signatureSrc && <Image className="signature" src={signatureSrc} />}
                    <ButtonSquare
                        block
                        color="info"
                        onClick={() => this.toggle.onToggleSignaturePad()}
                    >
                        Create Signature
                    </ButtonSquare>
                </React.Fragment>
            )
        }
    }

    toggle = {
        onToggleSignaturePad: () => {
            const { isignaturePad } = this.state
            this.setState({ isignaturePad: !isignaturePad })
        }
    }

    componentWillReceiveProps() {
        this.getJobById()
    }

    updateJob = async newJob => {
        const jobService = jobController()
        const res = await jobService.updatejob(newJob.attributes.id, newJob)
        if (res && res.data.status) {
            this.props.history.push('/home')
        } else {
            alert('Cannot update job.')
        }
    }

    getJobById = async () => {
        const { job } = context.state
        let schemaList = { type: 'object', properties: {} }
        let uiSchema = {}
        if (job) {
            //Set Photo
            if (job.photo) {
                this.setState({
                    coverPhoto: `data:image/jpeg;base64,${
                        job.photo.attributes.image
                    }`,
                    isPhoto: true
                })
            } else {
                this.setState({
                    coverPhoto: Common['addphoto.png'],
                    isPhoto: false
                })
            }
            job.propertygroup.map(items => {
                if (items.property.attributes) {
                    schemaList.properties[items.property.attributes.name] = {
                        type: 'string',
                        title: items.property.attributes.name,
                        default: items.property.attributes.data
                    }
                    if (items.property.attributes.type === 'paragraph') {
                        uiSchema[items.property.attributes.name] = {
                            'ui:widget': 'textarea'
                        }
                    } else if (items.property.attributes.type === 'date') {
                        schemaList.properties[
                            items.property.attributes.name
                        ] = {
                            type: 'string',
                            format: 'date',
                            default: items.property.attributes.data
                                ? items.property.attributes.data
                                : ''
                        }
                        uiSchema[items.property.attributes.name] = {
                            'ui:widget': 'date'
                        }
                    }
                } else {
                    items.property.map(items => {
                        schemaList.properties[items.attributes.name] = {
                            type: 'string',
                            title: items.attributes.name,
                            default: items.attributes.data
                        }
                        if (items.attributes.type === 'date') {
                            schemaList.properties[items.attributes.name] = {
                                type: 'string',
                                format: 'date',
                                default: items.attributes.data
                            }
                            uiSchema[items.attributes.name] = {
                                'ui:widget': 'date'
                            }
                        } else if (items.attributes.type === 'photo') {
                            uiSchema[items.attributes.name] = {
                                'ui:widget': 'signatureField'
                            }
                        }
                    })
                }
            })
            this.setState({
                schemaList,
                uiSchema,
                jobName: job.attributes.name
            })
        }
    }

    render() {
        const {
            schemaList,
            uiSchema,
            coverPhoto,
            isignaturePad,
            signatureRef,
            jobName,
            signatureSrc
        } = this.state
        const styleField = { signatureField: this.form.onRenderSignature }
        return (
            <React.Fragment>
                <Row>
                    <Col className="mt-3">
                        <Flex>
                            <Flex.Left>
                                <Text>
                                    <Text.h3 weight="bold">{jobName}</Text.h3>
                                </Text>
                            </Flex.Left>
                            <ButtonSquare color="outline-info">
                                <FontAwesomeIcon icon={faFileExport} /> Export
                            </ButtonSquare>
                            <ButtonSquare color="outline-success">
                                <FontAwesomeIcon icon={faFilePdf} /> Report
                            </ButtonSquare>
                            <ButtonSquare
                                color="primary"
                                onClick={() => this.form.onClickSaveJob()}
                            >
                                <FontAwesomeIcon icon={faCheck} /> Save
                            </ButtonSquare>
                            <ButtonSquare
                                color="danger"
                                onClick={() => this.form.onClickCancelJob()}
                            >
                                <FontAwesomeIcon icon={faTimes} /> Cancel
                            </ButtonSquare>
                        </Flex>
                    </Col>
                </Row>
                <hr />
                <Container className="mt-3">
                    <Row className="justify-content-md-center">
                        <Card
                            color="secondary"
                            style={{
                                width: '100%',
                                height: '450px',
                                textAlign: 'center'
                            }}
                        >
                            <Image
                                src={coverPhoto}
                                width="650px"
                                height="450px"
                            />
                            <InputFile
                                name="coverPhotoUpload"
                                id="coverPhotoUpload"
                                accept="image/*"
                                onChange={this.form.onClickUploadPhoto}
                            />
                        </Card>
                    </Row>
                    <hr />
                    <Text>
                        <Text.p align="left" color="red">
                            {' '}
                            * Please click submit button or press enter before
                            save.
                        </Text.p>
                    </Text>
                    {typeof schemaList !== 'undefined' && (
                        <Form
                            key={'0'}
                            schema={schemaList}
                            uiSchema={uiSchema}
                            widgets={styleField}
                            onSubmit={this.form.onSubmitSaveJob}
                            noValidate={true}
                        />
                    )}
                </Container>
                <Modal
                    isOpen={isignaturePad}
                    toggle={this.toggle.onToggleSignaturePad}
                >
                    <ModalBody>
                        <SignaturePad ref={signatureRef} />
                    </ModalBody>
                    <ModalFooter>
                        <ButtonSquare
                            color="primary"
                            onClick={() => this.form.onClickSaveSignature()}
                        >
                            Save
                        </ButtonSquare>{' '}
                        <ButtonSquare
                            color="danger"
                            onClick={() => this.form.onClickClearSignature()}
                        >
                            Clear
                        </ButtonSquare>
                        <ButtonSquare
                            color="secondary"
                            onClick={() => this.toggle.onToggleSignaturePad()}
                        >
                            Close
                        </ButtonSquare>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        )
    }
}
