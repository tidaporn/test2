import React from 'react'
import { SidebarStyle } from './style'

export class Sidebar extends React.Component {
    render() {
        const { className, ptop, children } = this.props
        return (
            <React.Fragment>
                <SidebarStyle className={className} ptop={ptop}>{children}</SidebarStyle>
            </React.Fragment>
        )
    }
}

SidebarStyle.defaultProps = {
    ptop: '0'
}
