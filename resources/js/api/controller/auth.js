import apiService from '../service'
import { BASE_API } from '../config'

const headerJson = { 'Content-Type': 'application/json' }
export const authController = () => {
    const http = apiService()
    return {
        loginVerify: async () => {
            return await http.get({
                url: `${BASE_API}/login`,
                headers: headerJson
            })
        }
    }
}
