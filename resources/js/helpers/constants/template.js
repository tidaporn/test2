export const template = [
    { id: 0, value: 'english', name: 'English Standard v2.0' },
    { id: 1, value: 'chinese', name: 'Chinese Standard v2.0' },
    { id: 2, value: 'arabic', name: 'Arabic Standard v2.0' },
    { id: 3, value: 'franch', name: 'Franch Standard v2.0' },
    { id: 4, value: 'hindi', name: 'Hindi Standard v2.0' },
    { id: 5, value: 'spanish', name: 'Spanish Standard v2.0' }
]

export const findTemplateByName = name => {
    return template.find(item => name.search(item.value)).value
}
