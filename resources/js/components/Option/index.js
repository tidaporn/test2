import React from 'react'
import { OptionStyle } from './style'

export class Option extends React.Component {
    render() {
        const { value, children } = this.props
        return (
            <React.Fragment>
                <OptionStyle value={value}>{children}</OptionStyle>
            </React.Fragment>
        )
    }
}
