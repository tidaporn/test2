import styled from 'styled-components'

export const TableTbodyStyle = styled.tbody``

export const TableTdStyle = styled.td`
    width: ${props => props.width};
`
export const TableTrStyle = styled.tr``
export const TableThStyle = styled.th``
