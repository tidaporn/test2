import React from 'react'
import { ImageStyle } from './style'

export class Image extends React.Component {
    render() {
        const { className, src, alt, width, height, children } = this.props
        return (
            <React.Fragment>
                <ImageStyle
                    className={className}
                    src={src}
                    alt={alt}
                    width={width}
                    height={height}
                >
                    {children}
                </ImageStyle>
            </React.Fragment>
        )
    }
}

ImageStyle.defaultProps = {
    width: '100%',
    height: '100%'
}
