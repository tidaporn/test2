import React from 'react'
import { Provider } from './context'
import { RouteContainer } from './containers'
import { Route, BrowserRouter as Router } from 'react-router-dom'

export default class App extends React.Component {
    render() {
        return (
            <Provider app={this}>
                <Router>
                    <Route path="/" component={RouteContainer} />
                </Router>
            </Provider>
        )
    }
}
