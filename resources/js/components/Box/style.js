import styled from 'styled-components'
import { Card, CardBody, CardTitle } from 'reactstrap'

export const BoxStyle = styled(Card)`
    && {
        border-radius: 0px;
        border: 0px;
    }
`
export const BoxBodyStyle = styled(CardBody)``
export const BoxTitleStyle = styled(CardTitle)`
    && {
        text-transform: uppercase;
        color: rgb(19, 19, 129);
        font-weight: 700 !important;
        font-size: 1.5rem;
    }
`
