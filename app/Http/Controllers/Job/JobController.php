<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Job\TemplateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Log;
use Nathanmac\Utilities\Parser\Parser;
use Spatie\ArrayToXml\ArrayToXml;

class JobController extends Controller
{
    public function getAllJob()
    {
        $parser = new Parser();
        $xmlCollection = collect([]);
        try {
            $file = Storage::get('User1/index.xml');
            $xml = $parser->xml($file);
            return response()->json(['status' => true, 'message' => $xml], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => true, 'message' => 'No job.'], 200);

        }

    }

    public function getJobById($id)
    {
        $directory = 'User1/Job';
        $files = Storage::allFiles($directory);
        foreach ($files as $file) {
            $rawData = Storage::get($file);
            $parser = new Parser();
            $dataList = $parser->xml($rawData);
            if ($id === $dataList['attributes']['id']) {
                return response()->json(['status' => true, 'message' => $dataList], 200);
            }
        }
        return response()->json(['status' => false, 'message' => 'Something wrong'], 500);
    }

    public function addJob(Request $request)
    {
        $req = $request->json()->all();
        if (isset($req['job']) && isset($req['template'])) {
            $template = new TemplateController();
            $template->getTemplate($req['template']);
            $getFileName = explode('/', $template->getTemplateName($req['template'], 'user'));
            $getTemplate = Cache::get('user' . $req['template']);
            $this->IncrementId();
            $getId = Cache::get('id');
            $attributes = array('attributes' => array('id' => $getId,
                'name' => $req['job'],
                'coordinate' => '',
                'permission' => 'write',
                'template' => $getFileName[2],
            ));
            Log::info($getFileName);
            unset($getTemplate['job']['system']);
            $job = array_merge($attributes, $getTemplate['job']);
            $jobData = ArrayToXml::convert($job);
            $this->updateListJob($jobData, $req['job']);
            // Storage::cloud()->put('10LZmLevUv2ntFa_w39sslghN4lB2Mt41/' . $req['job'] . '.nsp', $jobData);
            return response()->json(['status' => true], 200);
        } else {
            return response()->json(['status' => false, 'message' => 'Something wrong'], 500);
        }
    }

    public function updateJob($id, Request $request)
    {
        $req = $request->json()->all();
        $directory = 'User1/Job';
        $files = Storage::allFiles($directory);
        foreach ($files as $file) {
            $rawData = Storage::get($file);
            $parser = new Parser();
            $dataList = $parser->xml($rawData);
            $jobData = ArrayToXml::convert($req);
            if ($id === $dataList['attributes']['id']) {
                $getJob = Storage::get('User1/index.xml');
                $jobList = $parser->xml($getJob);
                foreach ($jobList['job'] as $jobKey => $job) {
                    if ($id === $job['attributes']['id']) {
                        unset($jobList['job'][$jobKey]);
                    }
                }
                if (empty($jobList['job'][0])) {
                    $jobXml = ArrayToXml::convert($jobList['job'][1]);
                } else {
                    $jobXml = ArrayToXml::convert($jobList['job'][0]);
                }
                // Storage::delete('User1/index.xml');
                // $this->updateListJob($jobXml, null);
                // $this->updateListJob($jobData, null);
                Log::info($jobList['job'][1]);
                //return response()->json(['status' => true, 'message' => 'Updated!'], 200);
            }
        }
        return response()->json(['status' => false, 'message' => 'Something wrong'], 500);

    }

    public function searchJob(Request $request)
    {
        $keyword = $request->query('job');
        $searchResult = collect([]);
        if (Storage::get('User1/index.xml')) {
            $getFile = Storage::get('User1/index.xml');
            $parser = new Parser();
            $jobList = $parser->xml($getFile);
            if (!empty($keyword)) {
                foreach ($jobList['job'] as $job) {
                    if ($keyword === $job['attributes']['name']
                        || $keyword === $job['propertygroup'][1]['property'][0]['attributes']['data']
                        || $keyword === $job['propertygroup'][1]['property'][2]['attributes']['data']) {
                        $searchResult->push($job);
                    }
                }
                return response()->json(['status' => true, 'message' => $searchResult], 200);
            }
        } else {
            return response()->json(['status' => false, 'message' => []], 200);
        }
    }

    public function deleteJob($id)
    {

        $files = Storage::allFiles('User1/Job');
        $getFile = Storage::get('User1/index.xml');
        $xml = new \SimpleXMLElement($getFile);
        foreach ($xml->job as $job) {
            if ($job->attributes()->id == $id) {
                $dom = dom_import_simplexml($job);
                $dom->parentNode->removeChild($dom);

            }
        }

        foreach ($files as $file) {
            $parser = new Parser();
            $rawData = Storage::get($file);
            $dataList = $parser->xml($rawData);
            if ($id === $dataList['attributes']['id']) {
                Storage::delete('User1/index.xml');
                $prettierXml = $this->PrettierXml($xml->asXML());
                Storage::disk('local')->put('User1/index.xml', $prettierXml);
                Storage::delete('User1/Job/' . $dataList['attributes']['name'] . '.nsp');
                return response()->json(['status' => true, 'message' => 'Delete!'], 200);
            }
        }
        return response()->json(['status' => false, 'message' => 'Cannot Delete'], 500);
    }

    public function cloneJob($id, Request $request)
    {
        $req = $request->json()->all();
        $this->IncrementId();
        $getId = Cache::get('id');

        if (isset($req['job'])) {
            $req['job']['id'] = $getId;
            $files = Storage::allFiles('User1/Job');
            foreach ($files as $file) {
                $rawData = Storage::get($file);
                $parser = new Parser();
                $dataList = $parser->xml($rawData);
                Log::info($dataList['attributes']['id']);
                if ($id === $dataList['attributes']['id']) {
                    $dataList['attributes']['id'] = $getId;
                    $jobData = ArrayToXml::convert($dataList);
                    $this->updateListJob($jobData, $dataList['attributes']['name']);
                    Storage::put('User1/Job/' . $dataList['attributes']['name'] . '.nsp', $jobData);
                    return response()->json(['status' => true, 'message' => 'Clone!'], 200);
                }
            }
        } else {
            return response()->json(['status' => false, 'message' => []], 500);

        }

    }

    public function ImportJob(Request $request)
    {
        $req = $request->json()->all();
        if (isset($req['data']) && isset($req['name'])) {
            $base64File = str_replace('data:application/octet-stream;base64,', '', $req['data']);
            $data = base64_decode($base64File);
            $name = str_replace('.nsp', '', $req['name']);
            $this->updateListJob($data, $name);
            return response()->json(['status' => true, 'message' => 'Success Import Job'], 200);
        }
        return response()->json(['status' => false, 'message' => 'Cannot Import Job'], 500);

    }
    private function updateListJob($jobData, $jobName)
    {
        if (Storage::disk('local')->exists('User1/index.xml') && !empty(Storage::get('User1/index.xml'))) {
            $rawData = Storage::get('User1/index.xml');
            $app = new \SimpleXMLElement($rawData);
        } else {
            $app = new \SimpleXMLElement("<app name='' template='Default__Default__1.0__Default'></app>");
        }
        $job = preg_replace('!^[^>]+>(\r\n|\n)!', '', $jobData);
        $newJob = new \SimpleXMLElement($job);
        $this->xml_adopt($app, $newJob);
        $prettierXml = $this->PrettierXml($app->asXML());
        Storage::disk('local')->put('User1/index.xml', $prettierXml);
        if (!empty($jobName)) {
            Storage::disk('local')->put('User1/Job/' . $jobName . '.nsp', $jobData);
        }

    }

    private function xml_adopt($root, $new)
    {
        $node = $root->addChild($new->getName(), (string) $new);
        foreach ($new->attributes() as $attr => $value) {
            $node->addAttribute($attr, $value);
        }
        foreach ($new->children() as $ch) {
            $this->xml_adopt($node, $ch);
        }
    }

    private function PrettierXml($xml)
    {
        $domxml = new \DOMDocument();
        $domxml->preserveWhiteSpace = false;
        $domxml->formatOutput = true;
        $domxml->loadXML($xml);
        return $domxml->saveXML();

    }

    private function IncrementId()
    {
        if (Cache::has('id')) {
            $id = Cache::get('id') + 1;
            $idCache = Cache::forever('id', $id);
        } else {
            $idCache = Cache::forever('id', 1);
        }
    }

    private function getAllFileXml()
    {
        $paths = $this->getPath();
        $collection = collect([]);
        foreach ($paths as $path) {
            $readStream = Storage::cloud()->getDriver()->readStream($path);
            $rawData = stream_get_contents($readStream);
            $collection->push(['data' => $rawData, 'path' => $path]);
        }
        return $collection;
    }

    private function getPath()
    {
        $rootFolder = 'User1';
        $contents = collect(Storage::cloud()->listContents('/', false));
        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $rootFolder)
            ->first();
        if (!$dir) {
            return 'No such folder!';
        }
        $dirFolders = collect(Storage::cloud()->listContents($dir['path'], false))
            ->where('type', '=', 'dir');
        $listPath = $dirFolders->mapWithKeys(function ($file) {
            $folderName = $file['filename'] . '.' . $file['extension'];
            $path = $file['path'];
            return [$folderName => $path];
        });
        $jobFolder = $listPath['Job.'];
        $dir = '/' . $jobFolder;
        $recursive = false;
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
        $savePath = collect([]);
        $listContents = json_decode($contents, true);
        foreach ($listContents as $list => $value) {
            if (!str_contains($value['name'], '__') && !str_contains($value['name'], '_DS_Store')) {
                $savePath->push($value['basename']);
            }
        }
        return $savePath;
    }
}
