import React from 'react'
import { LoadingStyle, LoadingItemStyle } from './style'

export class Loading extends React.Component {
    render() {
        const { showLoading } = this.props
        return (
            <React.Fragment>
                {showLoading && (
                    <LoadingStyle>
                        <LoadingItemStyle>
                            <div />
                            <div />
                            <div />
                            <div />
                            <div />
                            <div />
                            <div />
                            <div />
                        </LoadingItemStyle>
                    </LoadingStyle>
                )}
            </React.Fragment>
        )
    }
}

Loading.defaultProps = {
    showLoading: false
}
