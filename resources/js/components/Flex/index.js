import React from 'react'

const FlexLeft = props => {
    const { children } = props
    return (
        <React.Fragment>
            <div className="mr-auto">{children}</div>
        </React.Fragment>
    )
}

const FlexRight = props => {
    const { children } = props
    return (
        <React.Fragment>
            <div className="ml-auto">{children}</div>
        </React.Fragment>
    )
}

export class Flex extends React.Component {
    static Right = FlexRight
    static Left = FlexLeft
    render() {
        const { children } = this.props
        return (
            <React.Fragment>
                {<div className="d-flex align-items-baseline">{children}</div>}
            </React.Fragment>
        )
    }
}

export class FlexInline extends React.Component {
    static Right = FlexRight
    static Left = FlexLeft
    render() {
        const { children } = this.props
        return (
            <React.Fragment>
                {<div className="d-inline-flex">{children}</div>}
            </React.Fragment>
        )
    }
}
