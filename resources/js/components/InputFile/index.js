import React from 'react'
import { InputFileStyle, InputFileLabelStyle } from './style'

export class InputFile extends React.Component {
    render() {
        const { name, id, accept, onChange, children } = this.props
        return (
            <React.Fragment>
                <InputFileStyle
                    type="file"
                    name={name}
                    id={id}
                    accept={accept}
                    className={name}
                    onChange={onChange}
                />
                {children ? (
                    <InputFileLabelStyle
                        type="button"
                        className="sc-bdVaJa EKfNO btn btn-outline-secondary btn-md"
                        htmlFor={name}
                    >
                        {children}
                    </InputFileLabelStyle>
                ) : null}
            </React.Fragment>
        )
    }
}
