import moment from 'moment'

export const convertDateTime = date => {
    if (date) return moment(date).format('ll')
}
