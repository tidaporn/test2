import React from 'react'
import {
    ButtonSquare,
    Box,
    Flex,
    Tables,
    Image,
    Text,
    Option,
    InputFile
} from '../../components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faSearch,
    faPlus,
    faFileImport,
    faEdit,
    faCopy,
    faTrashAlt,
    faClipboardList
} from '@fortawesome/free-solid-svg-icons'
import {
    Form,
    FormGroup,
    Label,
    CardBody,
    Input,
    Col,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap'
import { Common } from '../../assets'
import { template, convertDateTime } from '../../helpers'
import { jobController } from '../../api'
import _ from 'lodash'

export class HomeContainer extends React.Component {
    state = {
        jobName: '',
        searchInput: '',
        templateSelect: 'english',
        addModal: false,
        jobList: []
    }

    form = {
        onChangeInput: ev => {
            const { name, value } = ev.target
            this.setState({ [name]: value })
        },
        onChangeSelect: ev => {
            const { name, value } = ev.target
            this.setState({ [name]: value })
        },
        onChangeImportFile: ev => {
            const { files } = ev.target
            const file = files[0]
            const reader = new FileReader()
            if (files.length === 0) return
            if (file.name.includes('.nsp')) {
                reader.onload = () => {
                    this.importJob(file.name, reader.result)
                }
                reader.readAsDataURL(file)
            } else {
                alert('Invalid format file.')
            }
        },
        onClickAddJob: () => {
            const { jobName, addModal, templateSelect } = this.state
            const job = { job: jobName, template: templateSelect }
            this.addJob(job)
            this.setState({
                addModal: !addModal,
                jobName: '',
                templateSelect: 'english'
            })
        },
        onClickEditJob: id => {
            this.props.history.push(`/edit/job/${id}`)
        },
        onClickDeleteJob: id => {
            window.confirm('Confirm to delete?.')
                ? this.deleteJob(id)
                : null
        },
        onClickCloneJob: (id, job) => {
            this.cloneJob(id, job)
        },
        onClickSearchJob: () => {
            const { searchInput } = this.state
            this.searchJob(searchInput)
            this.setState({ searchInput: '' })
        }
    }
    toggle = {
        onClickToggleAddJob: () => {
            const { addModal } = this.state
            this.setState({ addModal: !addModal })
        }
    }

    componentDidMount() {
        this.getJob()
    }

    getJob = async () => {
        const jobService = jobController()
        const res = await jobService.getAllJob()
        if (res && res.data.status) {
            const job = res.data.message.job
            console.log('job', job)
            if (Array.isArray(job)) {
                this.setState({ jobList: job })
            } else if (typeof job === 'undefined') {
                this.setState({ jobList: [] })
            } else {
                this.setState({
                    jobList: [job]
                })
            }
        }
    }

    addJob = async data => {
        const jobService = jobController()
        const res = await jobService.createjob(data)
        if (res && res.data.status) {
            this.getJob()
        } else {
            alert('Cannot add job.')
        }
    }

    cloneJob = async (id, data) => {
        const jobService = jobController()
        const res = await jobService.cloneJob(id, data)
        if (res && res.data.status) {
            this.getJob()
        } else {
            alert('Cannot clone job.')
        }
    }

    deleteJob = async id => {
        const jobService = jobController()
        const res = await jobService.deletejob(id)
        if (res && res.data.status) {
            this.getJob()
        } else {
            alert('Cannot delete job.')
        }
    }

    importJob = async (name, file) => {
        const jobService = jobController()
        const res = await jobService.importJob(name, file)
        if (res && res.data.status) {
            this.getJob()
        } else {
            alert('Cannot import job.')
        }
    }

    searchJob = async data => {
        const jobService = jobController()
        const res = await jobService.searchjob(data)
        if (res && res.data.status) {
            const job = res.data.message
            console.log('search', job)
            if (Array.isArray(job)) {
                this.setState({ jobList: job })
            } else if (typeof job === 'undefined') {
                this.setState({ jobList: [] })
            } else {
                this.setState({
                    jobList: [job]
                })
            }
        }
    }
    render() {
        const {
            addModal,
            jobName,
            templateSelect,
            jobList,
            searchInput
        } = this.state
        return (
            <React.Fragment>
                <Box>
                    <Form>
                        <FontAwesomeIcon
                            icon={faSearch}
                            size="lg"
                            className="icon-search"
                        />
                        <Col sm="10">
                            <Input
                                type="text"
                                name="searchInput"
                                id="searchInput"
                                value={searchInput}
                                placeholder="Job name, Client name, Address"
                                autoComplete="off"
                                className="search-box"
                                onChange={this.form.onChangeInput}
                            />
                        </Col>
                    </Form>
                    <Flex.Right>
                        <ButtonSquare
                            color="primary"
                            size="lg"
                            onClick={() => this.form.onClickSearchJob()}
                        >
                            Search
                        </ButtonSquare>
                    </Flex.Right>
                </Box>
                <Box>
                    <Box.body>
                        <Flex>
                            <Box.title>Job List</Box.title>
                            <Flex.Right>
                                <InputFile
                                    name="buttonUpload"
                                    id="buttonUpload"
                                    onChange={this.form.onChangeImportFile}
                                >
                                    <FontAwesomeIcon icon={faFileImport} />{' '}
                                    Import File
                                </InputFile>
                                <ButtonSquare
                                    color="info"
                                    size="md"
                                    onClick={() =>
                                        this.toggle.onClickToggleAddJob()
                                    }
                                >
                                    <FontAwesomeIcon icon={faPlus} /> Add
                                </ButtonSquare>
                            </Flex.Right>
                        </Flex>
                    </Box.body>
                    <CardBody>
                        {jobList.length > 0 ? (
                            <Table responsive>
                                <Tables.tbody>
                                    {jobList
                                        .map((job, index) => {
                                            return (
                                                <Tables.tr key={index}>
                                                    <Tables.td width="5%">
                                                        {job.photo ? (
                                                            <Image
                                                                className="rounded"
                                                                src={`data:image/jpeg;base64,${
                                                                    job.photo
                                                                        .attributes
                                                                        .image
                                                                }`}
                                                                alt="thumbnail"
                                                                width="60px"
                                                                height="60px"
                                                            />
                                                        ) : (
                                                            <Image
                                                                className="rounded"
                                                                src={
                                                                    Common[
                                                                        'thumbnails.png'
                                                                    ]
                                                                }
                                                                alt="thumbnail"
                                                                width="60px"
                                                                height="60px"
                                                            />
                                                        )}
                                                    </Tables.td>
                                                    <Tables.td>
                                                        <Text align="left">
                                                            <Text.h5 weight="700">
                                                                {
                                                                    job
                                                                        .attributes
                                                                        .name
                                                                }
                                                            </Text.h5>
                                                            <Text.p color="grey">
                                                                {
                                                                    job
                                                                        .propertygroup[2]
                                                                        .property[0]
                                                                        .attributes
                                                                        .data
                                                                }
                                                            </Text.p>
                                                        </Text>
                                                    </Tables.td>
                                                    <Tables.td>
                                                        {convertDateTime(
                                                            job.propertygroup[0]
                                                                .property
                                                                .attributes.data
                                                        )}
                                                    </Tables.td>
                                                    <Tables.td>
                                                        <Text align="right">
                                                            <ButtonSquare
                                                                color="primary"
                                                                size="md"
                                                                onClick={() =>
                                                                    this.form.onClickEditJob(
                                                                        job
                                                                            .attributes
                                                                            .id
                                                                    )
                                                                }
                                                            >
                                                                <FontAwesomeIcon
                                                                    icon={
                                                                        faEdit
                                                                    }
                                                                />
                                                            </ButtonSquare>
                                                            <ButtonSquare
                                                                color="secondary"
                                                                size="md"
                                                                onClick={() =>
                                                                    this.form.onClickCloneJob(
                                                                        job
                                                                            .attributes
                                                                            .id,
                                                                        job
                                                                    )
                                                                }
                                                            >
                                                                <FontAwesomeIcon
                                                                    icon={
                                                                        faCopy
                                                                    }
                                                                />
                                                            </ButtonSquare>
                                                            <ButtonSquare
                                                                color="danger"
                                                                size="md"
                                                                onClick={() =>
                                                                    this.form.onClickDeleteJob(
                                                                        job
                                                                            .attributes
                                                                            .id
                                                                    )
                                                                }
                                                            >
                                                                <FontAwesomeIcon
                                                                    icon={
                                                                        faTrashAlt
                                                                    }
                                                                />
                                                            </ButtonSquare>
                                                        </Text>
                                                    </Tables.td>
                                                </Tables.tr>
                                            )
                                        })
                                        .reverse()}
                                </Tables.tbody>
                            </Table>
                        ) : (
                            <React.Fragment>
                                <Text>
                                    <FontAwesomeIcon
                                        className="text-center"
                                        size="10x"
                                        color="rgba(187, 187, 187, 0.452)"
                                        icon={faClipboardList}
                                    />
                                    <Text.h3 color="rgba(187, 187, 187, 0.452)">
                                        No Job to Display
                                    </Text.h3>
                                </Text>
                            </React.Fragment>
                        )}
                    </CardBody>
                </Box>
                <Modal
                    isOpen={addModal}
                    toggle={this.toggle.onClickToggleAddJob}
                    className={this.props.className}
                >
                    <ModalHeader
                        toggle={this.toggle.onClickToggleAddJob}
                        className="bg-primary text-white"
                    >
                        New Job
                    </ModalHeader>
                    <ModalBody>
                        <Input
                            type="text"
                            name="jobName"
                            id="jobName"
                            placeholder="Job Name"
                            value={jobName}
                            onChange={this.form.onChangeInput}
                        />
                        <Text align="left" className="mt-3">
                            <Text.p align="left">Template</Text.p>
                        </Text>
                        <Input
                            type="select"
                            name="templateSelect"
                            id="templateSelect"
                            value={templateSelect}
                            onChange={this.form.onChangeSelect}
                        >
                            {template.map(item => {
                                return (
                                    <Option key={item.id} value={item.value}>
                                        {item.name}
                                    </Option>
                                )
                            })}
                        </Input>
                        <ModalFooter>
                            <ButtonSquare
                                type="submit"
                                color="primary"
                                disabled={jobName === ''}
                                onClick={() => this.form.onClickAddJob()}
                            >
                                Save
                            </ButtonSquare>
                            <ButtonSquare
                                color="secondary"
                                onClick={() =>
                                    this.toggle.onClickToggleAddJob()
                                }
                            >
                                Cancel
                            </ButtonSquare>
                        </ModalFooter>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        )
    }
}
