import React from 'react'
import {
    TableTbodyStyle,
    TableTdStyle,
    TableTrStyle,
    TableThStyle
} from './style'

const TdStyle = props => {
    const { width, children } = props
    return (
        <React.Fragment>
            <TableTdStyle width={width}>{children}</TableTdStyle>
        </React.Fragment>
    )
}

const TrStyle = props => {
    const { children } = props
    return (
        <React.Fragment>
            <TableTrStyle>{children}</TableTrStyle>
        </React.Fragment>
    )
}

const ThStyle = props => {
    const { children } = props
    return (
        <React.Fragment>
            <TableThStyle>{children}</TableThStyle>
        </React.Fragment>
    )
}

const TableTbody = props => {
    const { children } = props
    return (
        <React.Fragment>
            <TableTbodyStyle>{children}</TableTbodyStyle>
        </React.Fragment>
    )
}

export class Tables extends React.Component {
    static tr = TrStyle
    static th = ThStyle
    static td = TdStyle
    static tbody = TableTbody
    render() {
        const { children } = this.props
        return <React.Fragment>{children}</React.Fragment>
    }
}

TdStyle.defaultProps = {
    width: '0%'
}
