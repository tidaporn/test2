import apiService from '../service'
import { BASE_API } from '../config'
const headerJson = {
    'Content-Type': 'application/json'
}
export const templateController = () => {
    const service = apiService()
    return {
        findTemplate: async name => {
            return await service.get({
                url: `${BASE_API}/template/${name}`,
                headers: headerJson
            })
        },
        findSystemTemplate: async name => {
            return await service.get({
                url: `${BASE_API}/template/system/${name}`,
                headers: headerJson
            })
        }
    }
}
