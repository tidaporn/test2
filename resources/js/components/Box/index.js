import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { BoxStyle, BoxTitleStyle, BoxBodyStyle } from './style'

const BoxBody = props => {
    const { children } = props
    return (
        <React.Fragment>
            <BoxBodyStyle>{children}</BoxBodyStyle>
        </React.Fragment>
    )
}

const BoxTitle = props => {
    const { children } = props
    return (
        <React.Fragment>
            <BoxTitleStyle>{children}</BoxTitleStyle>
        </React.Fragment>
    )
}

export class Box extends React.Component {
    static body = BoxBody
    static title = BoxTitle
    render() {
        const { children } = this.props
        return (
            <React.Fragment>
                <Container>
                    <Row className="justify-content-center mt-5">
                        <Col sm="8">
                            <BoxStyle>{children}</BoxStyle>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        )
    }
}
