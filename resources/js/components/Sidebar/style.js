import styled from 'styled-components'
import { Col } from 'reactstrap'

export const SidebarStyle = styled(Col)`
    && {
        background: white;
        position: sticky;
        position: -webkit-sticky;
        top: 0;
        z-index: 1;
        height: auto;
        padding-top: ${props => props.ptop};
    }
    &.non-sticky {
        position: relative;
    }
`
