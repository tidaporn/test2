import React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'
import {
    HomeContainer,
    NavbarContainer,
    LoginContainer,
    SidebarContainer
} from '../../containers'
import { context } from '../../context'
import { Loading } from '../../components'
import { getLocalStorage } from '../../helpers'

export class RouteContainer extends React.Component {
    state = { isShowLoading: false }
    constructor(props) {
        super(props)
        context.setContext({
            routeHistory: this.props.history
        })
    }

    componentWillReceiveProps() {
        const { isShowLoading } = context.state
        this.setState({ isShowLoading: isShowLoading })
    }
    render() {
        const { isShowLoading } = this.state
        const isLoggedIn = getLocalStorage('userAuth')
        return (
            <React.Fragment>
                <Loading showLoading={isShowLoading} />
                {isLoggedIn ? (
                    <React.Fragment>
                        <NavbarContainer />
                        <Switch>
                            <Route exact path="/home" component={HomeContainer} />
                            <Route
                                path="/edit/job/:jobId"
                                component={SidebarContainer}
                            />
                            <Redirect to="/home" />
                        </Switch>
                    </React.Fragment>
                ) : (
                    <Switch>
                        <Route exact path="/" component={LoginContainer} />
                        <Redirect to="/" />
                    </Switch>
                )}
            </React.Fragment>
        )
    }
}
