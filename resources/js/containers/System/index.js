import React from 'react'
import {
    Container,
    Row,
    Col,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    ListGroup,
    ListGroupItem,
    Input,
    Card
} from 'reactstrap'
import {
    InputFile,
    Image,
    ListItem,
    Text,
    Flex,
    Sidebar,
    ButtonSquare
} from '../../components'
import { Common } from '../../assets'
import Form from 'react-jsonschema-form'
import SchemaField from 'react-jsonschema-form/lib/components/fields/SchemaField'
import { jobController, templateController } from '../../api'
import { context } from '../../context'
import { MenuBarContainer } from '../../containers'

export class SystemContainer extends React.Component {
    state = {
        formList: [],
        itemList: [],
        partList: [],
        isAddPart: false,
        isAddItemPart: false,
        namePart: '',
        nameItemPart: '',
        subtitlePart: '',
        textItempart: '',
        multiPhoto: [],
        partSelectList: [],
        partItem: []
    }

    toggle = {
        onToggleAddPart: name => {
            const { isAddPart } = this.state
            this.setState({
                isAddPart: !isAddPart,
                namePart: name
            })
        },
        onToggleAddItemPart: () => {
            const { isAddItemPart } = this.state
            this.setState({ isAddItemPart: !isAddItemPart })
        }
    }

    form = {
        onSubmitPart: ({ formData }) => {
            const { template, job } = context.state
            const newJob = job
            const id = this.props.match.params.systemId
            template.job.system[id].property.map(item => {
                if (item.attributes) {
                    item.attributes.data = formData[item.attributes.name]
                }
                return item
            })
            newJob.system = template.job.system
            this.saveTemplate(newJob)
        },
        onChangeInput: ev => {
            const { name, value } = ev.target
            this.setState({ [name]: value })
        },
        onClickUploadPhoto: ev => {
            const { files } = ev.target
            const reader = new FileReader()
            if (files[0].type.includes('image')) {
                reader.onloadend = e => {
                    this.setState({
                        multiPhoto: [...this.state.multiPhoto, reader.result],
                        isPhoto: true
                    })
                }
                reader.readAsDataURL(files[0])
            } else {
                alert('Invalid image format.')
            }
        },
        onClickItem: name => {
            const { itemList } = this.state
            itemList.map(items => {
                items.item.map(item => {
                    if (item.attributes.name === name) {
                        console.log('vvv', items)
                        return this.setState({
                            partList: item.part,
                            partSelectList: item.part
                        })
                    }
                    return item
                })
                return items
            })
        },
        onClickSelectPart: () => {
            const { template } = context.state
            const objsList = template.objs.property
            let schemaItemList = { type: 'object', properties: {} }
            let uiSchemaItemList = {}
            objsList.map(obj => {
                schemaItemList.properties[obj.attributes.name] = {
                    type: 'string',
                    title: obj.attributes.name,
                    default: obj.attributes.data
                }
                if (obj.attributes.type === 'paragraph') {
                    uiSchemaItemList[obj.attributes.name] = {
                        'ui:widget': 'textarea'
                    }
                }
                if (obj.opt) {
                    obj.opt.map(item => {
                        console.log('asdsad', item)
                        return (schemaItemList.properties[
                            obj.attributes.name
                        ] = { enum: [item.attributes.name] })
                    })
                }
            })
            this.setState({ schemaItemList, uiSchemaItemList })
        },
        onClickSelectPartItem: item => {
            const { partItem } = this.state
            partItem.push(item)
            this.setState({
                [item.attributes.name]: !this.state[item.attributes.name],
                partItem: partItem
            })
        },
        onClickSaveAddPart: () => {
            const { isAddPart, namePart, partList, partItem } = this.state
            const newPartList = [...partList]
            newPartList.map(list => {
                if (list.attributes.name === namePart) {
                    list.obj = list.obj.concat(partItem)
                }
                return list
            })
            partItem.map(item => {
                return this.setState({
                    [item.attributes.name]: !this.state[item.attributes.name]
                })
            })
            this.setState({
                partSelectList: newPartList,
                isAddPart: !isAddPart
            })
        },
        onClickSaveAddItemPart: () => {
            const {
                namePart,
                partList,
                nameItemPart,
                subtitlePart,
                textItempart
            } = this.state
            partList.map(list => {
                if (list.attributes.name === namePart) {
                    list.obj.push({
                        attributes: {
                            name: nameItemPart,
                            subtitle: subtitlePart,
                            text: textItempart
                        }
                    })
                }
                return this.setState({
                    nameItemPart: '',
                    subtitlePart: '',
                    textItempart: ''
                })
            })
            this.toggle.onToggleAddItemPart()
        },
        systemField: props => {
            // const { systemList } = this.state
            // const name = systemList.property.map(items => {
            //     return items.attributes.name
            // })
            // return (
            //     <FlexInline>
            //         <SchemaField {...props} />
            //         {props.schema.properties[name].enum ? (
            //             <Flex.Right>
            //                 <ButtonSquare>asdasd</ButtonSquare>
            //             </Flex.Right>
            //         ) : null}
            //     </FlexInline>
            // )
        }
    }

    componentWillReceiveProps() {
        const { template } = context.state
        console.log('template', template)
        if (template) {
            const id = this.props.match.params.systemId
            this.retriveForm(template.job.system[id])
        }
    }

    componentDidMount() {
        const id = this.props.match.params.systemId
    }

    saveTemplate = async job => {
        const { jobId } = context.state
        console.log('sadasdsadasdasdasd', jobId)
        const jobService = jobController()
        const res = await jobService.updatejob(jobId, job)
        if (res) {
            console.log(res)
        } else {
            alert('Cannot add job.')
        }
    }
    retriveForm = async systemList => {
        let schemaList = { type: 'object', properties: {} }
        let uiSchema = {}
        let a = []
        if (systemList.length !== 0) {
            if (systemList.property.attributes) {
                const newSystemList = systemList.section.map(items => {
                    return items
                })
                this.setState({ itemList: newSystemList })
            } else {
                systemList.property.map(items => {
                    if (items.attributes) {
                        schemaList.properties[items.attributes.name] = {
                            type: items.attributes.type,
                            title: items.attributes.name,
                            default: items.attributes.data
                        }

                        if (items.attributes.type === 'radio') {
                            items.opt.map(opt => {
                                a.push(opt.attributes.name)
                                schemaList.properties[items.attributes.name] = {
                                    enum: a
                                }
                            })
                        } else if (items.attributes.type === 'paragraph') {
                            schemaList.properties[items.attributes.name] = {
                                type: 'string'
                            }
                            uiSchema[items.attributes.name] = {
                                'ui:widget': 'textarea'
                            }
                        }
                    }
                    return items
                })
            }
            this.setState({ schemaList, uiSchema })
        }
    }

    render() {
        const {
            uiSchema,
            schemaList,
            itemList,
            partList,
            isAddPart,
            namePart,
            multiPhoto,
            isAddItemPart,
            nameItemPart,
            subtitlePart,
            textItempart,
            schemaItemList,
            uiSchemaItemList,
            partSelectList,
            formList
        } = this.state
        console.log('y', partList)
        const fields = { SchemaField: this.form.systemField }
        return (
            <React.Fragment>
                <MenuBarContainer>
                    {itemList.length > 0 ? (
                        <Container fluid>
                            <Row className="flex-xl-nowrap">
                                <Sidebar>
                                    <ListGroup>
                                        {itemList.map((items, index) => {
                                            return (
                                                <React.Fragment key={index}>
                                                    <Text>
                                                        <Text.p
                                                            weight="bold"
                                                            align="left"
                                                        >
                                                            {
                                                                items.attributes
                                                                    .name
                                                            }
                                                        </Text.p>
                                                    </Text>
                                                    {items.item.map(
                                                        (item, index) => {
                                                            return (
                                                                <ListItem
                                                                    key={index}
                                                                    tag="a"
                                                                    href="#"
                                                                    onClick={() =>
                                                                        this.form.onClickItem(
                                                                            item
                                                                                .attributes
                                                                                .name
                                                                        )
                                                                    }
                                                                >
                                                                    {
                                                                        item
                                                                            .attributes
                                                                            .name
                                                                    }
                                                                </ListItem>
                                                            )
                                                        }
                                                    )}
                                                </React.Fragment>
                                            )
                                        })}
                                    </ListGroup>
                                </Sidebar>
                                <Col sm="3">
                                    <Sidebar className="non-sticky">
                                        <ListGroup>
                                            {partSelectList.length > 0 &&
                                                partSelectList.map(
                                                    (item, index) => {
                                                        return (
                                                            <React.Fragment
                                                                key={index}
                                                            >
                                                                <Flex>
                                                                    <Text>
                                                                        <Text.p
                                                                            weight="bold"
                                                                            align="left"
                                                                        >
                                                                            {
                                                                                item
                                                                                    .attributes
                                                                                    .name
                                                                            }
                                                                        </Text.p>
                                                                    </Text>
                                                                    <Flex.Right>
                                                                        <ButtonSquare
                                                                            size="sm"
                                                                            color="outline-primary"
                                                                            onClick={() =>
                                                                                this.toggle.onToggleAddPart(
                                                                                    item
                                                                                        .attributes
                                                                                        .name
                                                                                )
                                                                            }
                                                                        >
                                                                            Add
                                                                        </ButtonSquare>
                                                                    </Flex.Right>
                                                                </Flex>
                                                                <ListGroup
                                                                    flush
                                                                >
                                                                    {item.obj
                                                                        .length >
                                                                        0 &&
                                                                        item.obj.map(
                                                                            (
                                                                                list,
                                                                                index
                                                                            ) => {
                                                                                return (
                                                                                    <React.Fragment
                                                                                        key={
                                                                                            index
                                                                                        }
                                                                                    >
                                                                                        <ListGroupItem
                                                                                            tag="a"
                                                                                            href="#"
                                                                                            onClick={() =>
                                                                                                this.form.onClickSelectPart()
                                                                                            }
                                                                                        >
                                                                                            {
                                                                                                list
                                                                                                    .attributes
                                                                                                    .name
                                                                                            }{' '}
                                                                                            <br />
                                                                                            <Text.span
                                                                                                color="grey"
                                                                                                align="left"
                                                                                            >
                                                                                                {
                                                                                                    list
                                                                                                        .attributes
                                                                                                        .text
                                                                                                }
                                                                                            </Text.span>
                                                                                        </ListGroupItem>
                                                                                    </React.Fragment>
                                                                                )
                                                                            }
                                                                        )}
                                                                </ListGroup>
                                                            </React.Fragment>
                                                        )
                                                    }
                                                )}
                                        </ListGroup>
                                    </Sidebar>
                                </Col>
                                <Col sm="6">
                                    {typeof schemaItemList !== 'undefined' && (
                                        <React.Fragment>
                                            <Card
                                                color="secondary"
                                                style={{
                                                    width: '100%',
                                                    height: '200px',
                                                    textAlign: 'center'
                                                }}
                                            >
                                                {multiPhoto.length > 0 ? (
                                                    multiPhoto.map(
                                                        (photo, index) => {
                                                            return (
                                                                <Image
                                                                    key={index}
                                                                    className="imgSlide"
                                                                    src={photo}
                                                                    width="50%"
                                                                    height="200px"
                                                                />
                                                            )
                                                        }
                                                    )
                                                ) : (
                                                    <React.Fragment>
                                                        <Image
                                                            className="imgSlide"
                                                            src={
                                                                Common[
                                                                    'addphoto.png'
                                                                ]
                                                            }
                                                            width="50%"
                                                            height="200px"
                                                        />
                                                        <InputFile
                                                            name="objPartUpload"
                                                            id="objPartUpload"
                                                            accept="image/*"
                                                            onChange={
                                                                this.form
                                                                    .onClickUploadPhoto
                                                            }
                                                        />
                                                    </React.Fragment>
                                                )}
                                            </Card>
                                            <hr />
                                            <Form
                                                onSubmit={
                                                    this.form.onSubmitPart
                                                }
                                                schema={schemaItemList}
                                                uiSchema={uiSchemaItemList}
                                                noValidate={true}
                                            >
                                                <button
                                                    type="submit"
                                                    style={{
                                                        visibility: 'hidden'
                                                    }}
                                                >
                                                    Submit
                                                </button>
                                            </Form>
                                        </React.Fragment>
                                    )}
                                </Col>
                            </Row>
                        </Container>
                    ) : (
                        typeof schemaList !== 'undefined' && (
                            <React.Fragment>
                                <Form
                                    key={'0'}
                                    schema={schemaList}
                                    uiSchema={uiSchema}
                                    noValidate={true}
                                    onSubmit={this.form.onSubmitPart}
                                >
                                    <button
                                        type="submit"
                                        style={{ visibility: 'hidden' }}
                                    >
                                        Submit
                                    </button>
                                </Form>
                            </React.Fragment>
                        )
                    )}
                </MenuBarContainer>
                <Modal isOpen={isAddPart} toggle={this.toggle.onToggleAddPart}>
                    <ModalHeader>
                        Select Item{' '}
                        <ButtonSquare
                            color="outline-primart"
                            onClick={() => this.toggle.onToggleAddItemPart()}
                        >
                            Add
                        </ButtonSquare>{' '}
                        <ButtonSquare
                            color="primary"
                            onClick={() => this.form.onClickSaveAddPart()}
                        >
                            Save
                        </ButtonSquare>{' '}
                        <ButtonSquare
                            color="danger"
                            onClick={() => this.toggle.onToggleAddPart()}
                        >
                            Cancel
                        </ButtonSquare>
                    </ModalHeader>
                    <ModalBody>
                        <ListGroup flush>
                            {partList.length > 0 &&
                                partList.map(item => {
                                    if (item.attributes.name === namePart) {
                                        return item.obj.map((list, index) => {
                                            return (
                                                <React.Fragment key={index}>
                                                    <ListGroupItem
                                                        tag="a"
                                                        href="#"
                                                        active={
                                                            this.state[
                                                                list.attributes
                                                                    .name
                                                            ]
                                                        }
                                                        onClick={() =>
                                                            this.form.onClickSelectPartItem(
                                                                list
                                                            )
                                                        }
                                                    >
                                                        {list.attributes.name}{' '}
                                                        <br />
                                                        <Text.span
                                                            color="grey"
                                                            align="left"
                                                        >
                                                            {
                                                                list.attributes
                                                                    .text
                                                            }
                                                        </Text.span>
                                                    </ListGroupItem>
                                                </React.Fragment>
                                            )
                                        })
                                    }
                                })}
                        </ListGroup>
                    </ModalBody>
                </Modal>
                <Modal
                    isOpen={isAddItemPart}
                    toggle={this.toggle.onToggleAddItemPart}
                >
                    <ModalHeader>Add Item</ModalHeader>
                    <ModalBody>
                        <Input
                            type="text"
                            name="nameItemPart"
                            id="nameItemPart"
                            placeholder="Name"
                            value={nameItemPart}
                            onChange={this.form.onChangeInput}
                        />
                        <Input
                            type="text"
                            name="subtitlePart"
                            id="subtitlePart"
                            placeholder="Subtitle (Optional)"
                            value={subtitlePart}
                            onChange={this.form.onChangeInput}
                        />
                        <Input
                            type="textarea"
                            name="textItempart"
                            id="textItempart"
                            placeholder="Text (Optional)"
                            value={textItempart}
                            onChange={this.form.onChangeInput}
                        />
                    </ModalBody>
                    <ModalFooter>
                        <ButtonSquare
                            color="primary"
                            disabled={nameItemPart === ''}
                            onClick={() => this.form.onClickSaveAddItemPart()}
                        >
                            Save
                        </ButtonSquare>{' '}
                        <ButtonSquare
                            color="danger"
                            onClick={() => this.toggle.onToggleAddItemPart()}
                        >
                            Cancel
                        </ButtonSquare>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        )
    }
}
