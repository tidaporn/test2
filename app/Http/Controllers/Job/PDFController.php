<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Nathanmac\Utilities\Parser\Parser;
use PDF;

class PDFController extends Controller
{
    public function pdfGenerator($id, $template)
    {

        $jobData = $this->getJobById($id);
        $templateData = $this->pdfTemplate($template);
        $htmlData = $this->getRawHtml();
        $htmlList = collect([]);

        $rawData1 = Storage::get('Templates/ReportTheme/Standard__English__5__Standard v5/cover.htm');
        preg_match_all("/\#.*#/", $rawData1, $matches);
        foreach ($templateData['report']['propertygroup'] as $report) {
            foreach ($report['property'] as $att) {
                foreach ($matches[0] as $matche) {
                    if (str_contains($matche, $att['attributes']['name'])) {
                        $replaceSharp = str_replace('#', '', $matche);
                        $replaceSymbol = str_replace('(', '\(', $replaceSharp);
                        $replaceSymbol1 = str_replace(')', '\)', $replaceSymbol);
                        $pattern = "#" . $replaceSymbol1 . "(.*?)#";
                        $result = preg_replace("/#Report(.*?)#/", $att['attributes']['data'], $rawData1);
                    }
                }
            }
        }

        $pdf = PDF::loadHTML('pdf.invoice', $data);
        return $pdf->download('invoice.pdf');

        // foreach ($htmlData as $html) {
        //     preg_match_all("/\#.*#/", $html, $matches);
        //     foreach ($matches as $matche) {
        //         foreach ($matche as $value) {
        //             foreach ($templateData['report']['propertygroup'] as $report) {
        //                 foreach ($report['property'] as $att) {
        //                     if (str_contains($value, $att['attributes']['name'])) {
        //                         $html = str_replace($value, $att['attributes']['data'], $html);
        //                         $replaceSharp = str_replace('#', '', $value);
        //                         $replaceSymbol = str_replace('(', '\(', $replaceSharp);
        //                         $replaceSymbol1 = str_replace(')', '\)', $replaceSymbol);
        //                         $pattern = "#" . $replaceSymbol1 . "(.*?)#";
        //                         $result = preg_replace($pattern, $att['attributes']['data'], $html);
        //                         Log::info($result);

        //                         //Log::info($att['attributes']['name']);
        //                         //$htmlList->push($html);
        //                     }

        //                 }

        //             }
        //         }
        //     }

        // }

    }

    public function getJobById($id)
    {
        $directory = 'User1/Job';
        $files = Storage::allFiles($directory);
        foreach ($files as $file) {
            $rawData = Storage::get($file);
            $parser = new Parser();
            $dataList = $parser->xml($rawData);
            if ($id === $dataList['attributes']['id']) {
                return $dataList;
            }
        }
    }

    private function pdfTemplate($name)
    {
        $templateName = 'Standard__' . ucfirst($name) . '__3.0__Standard v3.0';
        $pathUser = 'User1/ReportTemplate/';
        if (!Storage::disk('local')->exists($pathUser . $templateName . '.xml')) {
            Storage::copy('Templates/ReportTemplate/' . $templateName . '.xml',
                $pathUser . $templateName . '.xml');
        }
        $parser = new Parser();
        $rawData = Storage::get($pathUser . $templateName . '.xml');
        $template = $parser->xml($rawData);
        return $template;
    }

    private function getRawHtml()
    {
        $directory = 'Templates/ReportTheme/Standard__English__5__Standard v5';
        $html = collect([]);
        $files = Storage::allFiles($directory);
        foreach ($files as $file) {
            $rawData = Storage::get($file);
            $html->push($rawData);
        }
        return $html;

    }
}
