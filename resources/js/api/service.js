import axios from 'axios'
import { showLoading } from '../helpers'
import * as config from './config'
import { cacheAdapterEnhancer, throttleAdapterEnhancer } from 'axios-extensions'

axios.interceptors.response.use(
    res => {
        return res
    },
    err => {
        if (err.response.status === 401) {
            window.location.href = config.BASE_API + '/login'
        }
        return Promise.reject(err)
    }
)

let configService = {}
const axiosSuccess = res => {
    showLoading(false)
    return res
}

const axiosError = err => {
    showLoading(false)
    return err
}

const handleConfigService = () => {
    configService.isShowLoading =
        configService.isShowLoading === undefined
            ? true
            : configService.isShowLoading
    showLoading(configService.isShowLoading)
}

const axiosService = () => {
    handleConfigService()
    return {
        get: (url, headers) => {
            return axios
                .get(url, {
                    headers,
                    withCredentials: true,
                    adapter: cacheAdapterEnhancer(axios.defaults.adapter)
                })
                .then(axiosSuccess)
                .catch(axiosError)
        },
        post: (url, params, headers) => {
            return axios
                .post(url, params, { headers, withCredentials: true })
                .then(axiosSuccess)
                .catch(axiosError)
        },
        patch: (url, params, headers) => {
            return axios
                .patch(url, params, { headers, withCredentials: true })
                .then(axiosSuccess)
                .catch(axiosError)
        },
        delete: (url, headers) => {
            return axios
                .delete(url, { headers, withCredentials: true })
                .then(axiosSuccess)
                .catch(axiosError)
        }
    }
}

const apiService = () => {
    return {
        get: ({ url, headers }) => axiosService().get(url, headers),
        post: ({ url, body, headers }) =>
            axiosService().post(url, body, headers),
        patch: ({ url, body, headers }) =>
            axiosService().patch(url, body, headers),
        delete: ({ url, headers }) => axiosService().delete(url, headers)
    }
}

export default apiService
