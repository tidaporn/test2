import styled from 'styled-components'

export const InputFileStyle = styled.input`
    &.buttonUpload {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    &.coverPhotoUpload {
        opacity: 0;
        width: 100%;
        height: 450px;
        margin: 0 auto;
        position: absolute;
        cursor: pointer;
        z-index: 0;
    }
    &.objPartUpload {
        opacity: 0;
        width: 50%;
        height: 200px;
        margin: 0;
        position: absolute;
        cursor: pointer;
        z-index: 0;
    }
`

export const InputFileLabelStyle = styled.label`
    vertical-align: top;
`
