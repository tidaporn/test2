<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Nathanmac\Utilities\Parser\Parser;

class TemplateController extends Controller
{
    public function getMainTemplate($name)
    {
        $parser = new Parser();
        $fileName = $this->getTemplateName($name, 'system');
        $templateFile = Storage::get($fileName);
        if (empty($templateFile)) {
            return response()->json(['status' => false, 'message' => 'Template not found.'], 404);
        } else {
            if (Cache::has($name)) {
                $result = Cache::get($name);
                return response()->json(['status' => true, 'message' => $result], 200);
            } else {
                $xmlToArray = $parser->xml($templateFile);
                Cache::forever($name, $xmlToArray);
                $result = Cache::get($name);
                return response()->json(['status' => true, 'message' => $result], 200);
            }
            return response()->json(['status' => false, 'message' => 'Something wrong'], 500);

        }

    }
    public function getTemplate($name)
    {
        $parser = new Parser();
        $fileName = $this->getTemplateName($name, 'user');
        if (empty($fileName)) {
            $fileName = $this->getTemplateName($name, 'system');
            $filePath = str_replace('Templates', 'User1', $fileName);
            Storage::copy($fileName, $filePath);
        } else {
            $templateFile = Storage::get($fileName);
            if (Cache::has('user' . $name)) {
                $result = Cache::get('user' . $name);
                return response()->json(['status' => true, 'message' => $result], 200);
            } else {
                $xmlToArray = $parser->xml($templateFile);
                Cache::forever('user' . $name, $xmlToArray);
                $result = Cache::get('user' . $name);
                return response()->json(['status' => true, 'message' => $result], 200);
            }
            return response()->json(['status' => false, 'message' => 'Something wrong'], 500);

        }
    }

    public function getTemplateName($name, $type)
    {
        if ($type === 'user') {
            $directory = 'User1/JobTemplate';

        } else {
            $directory = 'Templates/JobTemplate';

        }
        $files = Storage::allFiles($directory);
        foreach ($files as $file) {
            if (str_contains(strtolower($file), $name) || $file === $name) {
                return $templateFile = $file;
                break;
            }
        }
    }

}
