import React from 'react'
import { Row, Col } from 'reactstrap'
import { Flex, ButtonSquare, Text } from '../../components'
import { context } from '../../context'
import {
    faFileExport,
    faFilePdf,
    faCheck,
    faTimes
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { jobController } from '../../api'
export class MenuBarContainer extends React.Component {
    state = { jobName: '' }

    form = {
        onClickSave: () => {
            console.log('save')
            this.updateJob()
        }
    }
    componentWillReceiveProps() {
        const { job } = context.state
        console.log('prop', this.props.hitstory)
        if (job) {
            this.setState({ jobName: job.attributes.name })
        }
    }

    updateJob = async () => {
        const { job } = context.state
        const jobService = jobController()
        const res = await jobService.updatejob(job.attributes.id, job)
        if (res && res.data.status) {
            
        } else {
            alert('Cannot update job.')
        }
    }
    render() {
        const { children } = this.props
        const { jobName } = this.state
        return (
            <React.Fragment>
                <Row>
                    <Col className="mt-3">
                        <Flex>
                            <Flex.Left>
                                <Text>
                                    <Text.h3 weight="bold">{jobName}</Text.h3>
                                </Text>
                            </Flex.Left>
                            <ButtonSquare color="outline-info">
                                <FontAwesomeIcon icon={faFileExport} /> Export
                            </ButtonSquare>
                            <ButtonSquare color="outline-success">
                                <FontAwesomeIcon icon={faFilePdf} /> Report
                            </ButtonSquare>
                            <ButtonSquare
                                color="primary"
                                onClick={() => this.form.onClickSave()}
                            >
                                <FontAwesomeIcon icon={faCheck} /> Save
                            </ButtonSquare>
                            <ButtonSquare color="danger">
                                <FontAwesomeIcon icon={faTimes} /> Cancel
                            </ButtonSquare>
                        </Flex>
                        <hr />
                        {children}
                    </Col>
                </Row>
            </React.Fragment>
        )
    }
}
