const mix = require('laravel-mix')
// const webpack = require('webpack')
// const CompressionPlugin = require('compression-webpack-plugin')

mix.react('resources/js/index.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    // .webpackConfig({
    //     plugins: [
    //         new webpack.DefinePlugin({
    //             'process.env': {
    //                 NODE_ENV: JSON.stringify('production')
    //             }
    //         }),
    //         new webpack.optimize.UglifyJsPlugin(),
    //         new webpack.optimize.AggressiveMergingPlugin(),
    //         // new CompressionPlugin({
    //         //     asset: 'resources/js/index.js.gz',
    //         //     algorithm: 'gzip',
    //         //     threshold: 10240,
    //         //     minRatio: 0.8
    //         // })
    //     ]
    // })
