import styled from 'styled-components'

export const TextStyle = styled.div`
    text-align: ${props => props.align};
    margin-right: ${props => props.right};
    margin-left: ${props => props.left};
    align-self: ${props => props.self};
`

export const TextH1Style = styled.h1`
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
export const TextH2Style = styled.h2`
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
export const TextH3Style = styled.h3`
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
export const TextH4Style = styled.h4`
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
export const TextH5Style = styled.h5`
    font-size: 1.25rem;
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
export const TextPStyle = styled.p`
    margin-top: 10px;
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
export const TextSpanStyle = styled.span`
    text-align: ${props => props.align};
    color: ${props => props.color};
    font-weight: ${props => props.weight};
`
