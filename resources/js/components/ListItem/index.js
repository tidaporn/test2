import styled from 'styled-components'
import { ListGroupItem } from 'reactstrap'

export const ListItem = styled(ListGroupItem)`
    && {
        cursor: pointer;
        border: none;
        overflow: hidden;
        text-overflow: ellipsis;
        &.disabled {
            background: red;
        }
        &:hover {
            background: lightgrey;
        }
    }

`
