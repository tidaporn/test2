
function importAll(r) {
    let files = {};
    r.keys().forEach((item) => {
        files[item.replace('./', '')] = r(item);
    });
    return files;
}

export const Common = importAll(require.context('./common', false, /\.(png|jpe?g|gif|svg)$/));
export const System = importAll(require.context('./system', false, /\.(png|jpe?g|gif|svg)$/));