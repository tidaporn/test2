import styled from 'styled-components'
import { Button } from 'reactstrap'

export const ButtonSquare = styled(Button)`
    && {
        border-radius: 0px;
        margin-left: 10px;
    }
`
