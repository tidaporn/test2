import React from 'react'
import {
    Container,
    Row,
    Col,
    ListGroup,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from 'reactstrap'
import {
    Sidebar,
    Flex,
    Text,
    ButtonSquare,
    Image,
    ListItem
} from '../../components'
import { findTemplateByName } from '../../helpers'
import { System } from '../../assets'
import Form from 'react-jsonschema-form'
import { templateController, jobController } from '../../api'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { context } from '../../context'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { JobContainer, SystemContainer } from '../../containers'

export class SidebarContainer extends React.Component {
    state = {
        isEdit: false,
        isInspectionModal: false,
        inspectionList: [],
        pathName: this.props.match.url,
        jobId: this.props.match.params.jobId,
        inspectionSelectList: [],
        inspectionItem: []
    }

    inspection = {
        onClickEdit: () => {
            const { isEdit } = this.state
            this.setState({ isEdit: !isEdit })
        },
        onClickAdd: () => {
            const { isEdit, isInspectionModal } = this.state
            this.setState({ isInspectionModal: !isInspectionModal })
        },
        onClickSave: () => {
            const {
                isEdit,
                isInspectionModal,
                inspectionList,
                inspectionItem
            } = this.state
            const mergeArray = inspectionList.concat(inspectionItem)
            inspectionItem.map(item => {
                return this.setState({
                    [item.attributes.type]: !this.state[item.attributes.type]
                })
            })
            this.setState({
                inspectionSelectList: mergeArray,
                isInspectionModal: !isInspectionModal,
                isEdit: !isEdit
            })
        },
        onClickCancel: () => {
            const { isEdit } = this.state
            this.setState({ isEdit: !isEdit })
        },
        onClickSelectItem: item => {
            const { inspectionItem } = this.state
            inspectionItem.push(item)
            this.setState({
                [item.attributes.type]: !this.state[item.attributes.type],
                inspectionItem: inspectionItem
            })
        },
        onClickHideItem: item => {
            console.log('sss')
        },
        onClickRemoveItem: index => {
            const { inspectionSelectList } = this.state
            inspectionSelectList.splice(index, 1)
            this.setState({ inspectionSelectList })
        },
        onClickResetItem: item => {},
        onClickGoToSystem: index => {
            const { pathName } = this.state
            this.props.history.push(`${pathName}/system/${index}`)
        }
    }

    componentDidMount() {
        context.setContext({jobId: this.state.jobId})
        this.getJobById()
    }

    getTemplate = async name => {
        const templateService = templateController()
        const res = await templateService.findTemplate(name)
        const { template } = context.state
        if (res && res.data.status) {
            const data = res.data.message
            this.setState({
                inspectionList: data.job.system,
                inspectionSelectList: data.job.system
            })
            console.log(template)
            if (!template) {
                console.log('yrd')
                context.setContext({ template: data })
            }
        }
    }

    getJobById = async () => {
        const { jobId } = this.state
        const jobService = jobController()
        const res = await jobService.findByIdjob(jobId)
        if (res && res.data.status) {
            const data = res.data.message
            context.setContext({ job: data })
            this.getTemplate(findTemplateByName(data.attributes.template))
        }
    }

    render() {
        const {
            isEdit,
            inspectionList,
            inspectionSelectList,
            pathName,
            isInspectionModal
        } = this.state
        return (
            <React.Fragment>
                <Container fluid>
                    <Row className="flex-xl-nowrap">
                        <Sidebar ptop="20px">
                            <Flex>
                                <Text>
                                    <Text.span weight="bold">
                                        <FontAwesomeIcon icon={faSearch} />{' '}
                                        Inspection
                                    </Text.span>
                                </Text>
                                <Flex.Right>
                                    {!isEdit && (
                                        <ButtonSquare
                                            color="outline-primary"
                                            onClick={() =>
                                                this.inspection.onClickEdit()
                                            }
                                        >
                                            Edit
                                        </ButtonSquare>
                                    )}
                                    {isEdit && (
                                        <React.Fragment>
                                            <ButtonSquare
                                                color="outline-primary"
                                                onClick={() =>
                                                    this.inspection.onClickAdd()
                                                }
                                            >
                                                Add
                                            </ButtonSquare>
                                            <ButtonSquare
                                                color="primary"
                                                onClick={() =>
                                                    this.inspection.onClickSave()
                                                }
                                            >
                                                Save
                                            </ButtonSquare>
                                            <ButtonSquare
                                                color="danger"
                                                onClick={() =>
                                                    this.inspection.onClickCancel()
                                                }
                                            >
                                                Cancel
                                            </ButtonSquare>
                                        </React.Fragment>
                                    )}
                                </Flex.Right>
                            </Flex>
                            <hr />
                            <ListGroup>
                                {inspectionSelectList.length > 0 &&
                                    inspectionSelectList.map((item, index) => {
                                        return (
                                            <ListItem
                                                key={index}
                                                tag="a"
                                                onClick={() =>
                                                    this.inspection.onClickGoToSystem(
                                                        index
                                                    )
                                                }
                                                // className={`${item}`}
                                            >
                                                <Image
                                                    key={index}
                                                    width="40px"
                                                    height="40px"
                                                    src={
                                                        item.attributes.type ===
                                                        'building data'
                                                            ? System[
                                                                  `custom.png`
                                                              ]
                                                            : System[
                                                                  `${
                                                                      item
                                                                          .attributes
                                                                          .type
                                                                  }.png`
                                                              ]
                                                    }
                                                />
                                                {item.attributes.name}
                                                {isEdit && (
                                                    <React.Fragment>
                                                        <ButtonSquare
                                                            color="outline-secondary"
                                                            onClick={() =>
                                                                this.inspection.onClickHideItem(
                                                                    item
                                                                )
                                                            }
                                                        >
                                                            Hide
                                                        </ButtonSquare>
                                                        <ButtonSquare
                                                            color="outline-danger"
                                                            onClick={() =>
                                                                this.inspection.onClickRemoveItem()
                                                            }
                                                        >
                                                            Remove
                                                        </ButtonSquare>
                                                        <ButtonSquare
                                                            color="outline-warning"
                                                            onClick={() =>
                                                                this.inspection.onClickResetItem()
                                                            }
                                                        >
                                                            Reset
                                                        </ButtonSquare>
                                                    </React.Fragment>
                                                )}
                                            </ListItem>
                                        )
                                    })}
                            </ListGroup>
                        </Sidebar>
                        <Col sm="9">
                            {' '}
                            <Route
                                path={`${pathName}/system/:systemId`}
                                component={SystemContainer}
                            />
                            <Route
                                exact
                                path={pathName}
                                component={JobContainer}
                            />
                        </Col>
                    </Row>
                </Container>
                <Modal
                    isOpen={isInspectionModal}
                    toggle={this.inspection.onClickAdd}
                >
                    <ModalHeader>
                        Add Item Inspection{' '}
                        <ButtonSquare
                            color="primary"
                            onClick={() => this.inspection.onClickSave()}
                        >
                            Save
                        </ButtonSquare>{' '}
                        <ButtonSquare
                            color="danger"
                            onClick={() => this.inspection.onClickAdd()}
                        >
                            Cancel
                        </ButtonSquare>
                    </ModalHeader>
                    <ModalBody>
                        <ListGroup>
                            {inspectionList.length >= 0 &&
                                inspectionList.map((item, index) => {
                                    return (
                                        <ListItem
                                            key={index}
                                            tag="a"
                                            href="#"
                                            active={
                                                this.state[item.attributes.type]
                                            }
                                            onClick={() =>
                                                this.inspection.onClickSelectItem(
                                                    item
                                                )
                                            }
                                        >
                                            <Image
                                                key={index}
                                                width="40px"
                                                height="40px"
                                                src={
                                                    item.attributes.type ===
                                                    'building data'
                                                        ? System[`custom.png`]
                                                        : System[
                                                              `${
                                                                  item
                                                                      .attributes
                                                                      .type
                                                              }.png`
                                                          ]
                                                }
                                            />
                                            {item.attributes.name}
                                        </ListItem>
                                    )
                                })}
                        </ListGroup>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        )
    }
}
