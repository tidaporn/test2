<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('job', 'Job\JobController@getAllJob');
Route::get('search', 'Job\JobController@searchJob');
Route::get('template/{name}', 'Job\TemplateController@getTemplate');
Route::get('template/system/{name}', 'Job\TemplateController@getMainTemplate');
Route::get('pdf/{id}/{template}', 'Job\PDFController@pdfGenerator');
Route::get('job/{id}', 'Job\JobController@getJobById');

Route::post('job', 'Job\JobController@addJob');
Route::post('cloneJob/{id}', 'Job\JobController@cloneJob');
Route::post('importJob', 'Job\JobController@importJob');

Route::patch('job/{id}', 'Job\JobController@updateJob');
Route::delete('job/{id}', 'Job\JobController@deleteJob');
