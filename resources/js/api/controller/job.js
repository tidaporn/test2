import apiService from '../service'
import { BASE_API } from '../config'
const headerJson = {
    'Content-Type': 'application/json'
}
export const jobController = () => {
    const service = apiService()
    return {
        getAllJob: async () => {
            return await service.get({
                url: `${BASE_API}/job/`,
                headers: headerJson
            })
        },
        findByIdjob: async id => {
            return await service.get({
                url: `${BASE_API}/job/${id}`,
                headers: headerJson
            })
        },
        createjob: async data => {
            const params = JSON.stringify(data)
            return await service.post({
                url: `${BASE_API}/job`,
                body: params,
                headers: headerJson
            })
        },
        updatejob: async (id, data) => {
            const params = JSON.stringify(data)
            return await service.patch({
                url: `${BASE_API}/job/${id}`,
                body: params,
                headers: headerJson
            })
        },
        cloneJob: async (id, data) => {
            const params = JSON.stringify({ job: data })
            return await service.post({
                url: `${BASE_API}/cloneJob/${id}`,
                body: params,
                headers: headerJson
            })
        },
        searchjob: async data => {
            return await service.get({
                url: `${BASE_API}/search?job=${data}`,
                headers: headerJson
            })
        },
        deletejob: async id => {
            return await service.delete({
                url: `${BASE_API}/job/${id}`,
                headers: headerJson
            })
        },
        importJob: async (name,data) => {
            const params = JSON.stringify({name: name, data: data})
            return await service.post({
                url: `${BASE_API}/importJob`,
                body: params,
                headers: headerJson
            })
        }
    }
}
