import React from 'react'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap'
import { setLocalStorage } from '../../helpers'
export class NavbarContainer extends React.Component {
    state = { isOpen: false }

    form = {
        onClickLogout: () => {
            setLocalStorage('userAuth', false)
        }
    }
    toggle = {
        onClickToggleNavbar: () => {
            const { isOpen } = this.state
            this.setState({ isOpen: !isOpen })
        }
    }
    render() {
        const { isOpen } = this.state
        return (
            <React.Fragment>
                <Navbar color="primary" dark expand="md">
                    <NavbarBrand className="font-weight-bold" href="/home">
                        NSpectpro
                    </NavbarBrand>
                    <NavbarToggler
                        onClick={() => this.toggle.onClickToggleNavbar()}
                    />
                    <Collapse isOpen={isOpen} navbar>
                        {/* <Nav navbar>
                            <NavItem>
                                <NavLink href="/edit/job/:jobId">Job</NavLink>
                            </NavItem>
                        </Nav> */}
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink
                                    href="/login/"
                                    onClick={() => this.form.onClickLogout()}
                                >
                                    Logout
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </React.Fragment>
        )
    }
}
