import React from 'react'
import {
    Container,
    Row,
    Col,
    Button,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap'
import { setLocalStorage } from '../../helpers'
export class LoginContainer extends React.Component {
    form = {
        onClickLogin: () => {
            setLocalStorage('userAuth', true)
            this.props.history.push('/home')
        }
    }
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 8, offset: 2 }}>
                            <Form>
                                <FormGroup>
                                    <Label for="exampleEmail">Email</Label>
                                    <Input
                                        type="email"
                                        name="email"
                                        id="exampleEmail"
                                        placeholder="with a placeholder"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePassword">
                                        Password
                                    </Label>
                                    <Input
                                        type="password"
                                        name="password"
                                        id="examplePassword"
                                        placeholder="password placeholder"
                                    />
                                </FormGroup>
                                <Button
                                    onClick={() => this.form.onClickLogin()}
                                >
                                    Login
                                </Button>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        )
    }
}
