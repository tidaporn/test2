import styled from 'styled-components'

export const ImageStyle = styled.img`
    width: ${props => props.width};
    height: ${props => props.height};
    margin: 0 auto;
    &.imgSlide {
        margin: 0;
    }
    &.signature {
        width: 100%;
        margin-bottom: 10px;
        border-style: solid;
        border-width: 0.1ch;
    }
`
